Le code source du document se trouve dans le fichier
Note_synthese_WP34.Rmd.

Pour obtenir un document en format .docx, il faut utiliser le logiciel
R avec le package rmarkdown. Il suffit d'ouvrir une session de R et de
d�finir le dossier de travail � celui o� se trouvent les fichiers :

- Note_synthese_WP34.Rmd (qui contient le texte)
- CoERT-P_synthese.bibtex (qui contient les references biblio)

Ensuite, il faut utiliser la commande render de la maniere suivante

rmarkdown::render("Note_synthese_WP34.Rmd")
